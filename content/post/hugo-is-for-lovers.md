+++
banner = "banners/placeholder.png"
categories = ["Lorem"]
date = "2015-08-03T13:39:46+02:00"
menu = ""
tags = []
title = "L'ASSOCIATION"
+++

## Préface. L'histoire


## Acte 1. Les formations au jeu

![photo formations TEDUA](/images/formations tedua.jpg)

L'association TEDUA S'est mis à exister à partir du moment où des personnes souhaitant découvrir le jeu ont coopérer ensemble pour que cela leur soit possible. 60 élèves à ce jour ont pratiqué ou pratique l'enseignement de théâtre cette association propose.
Plusieurs formations possibles :<br>
**- atelier clown<br>
- stage du jeu<br>
- art thérapie<br>
- préparation aux concours**
<iframe width="560" height="315" src="https://www.youtube.com/embed/XrEOK27KugE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Acte 2. Les créations


## Acte 3. Les événements


## Acte 4. La vie associative
