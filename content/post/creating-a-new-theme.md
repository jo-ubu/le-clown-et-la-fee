+++
banner = "banners/placeholder.png"
menu = ""
tags = []
title = "L'ÉQUIPE"
+++

## Julie Hercberg

![photo julie hercberg](/images/julie pour présentation.jpeg)

« **Choisir Julie pour jouer la fée.**<br>Au-delà de la beauté de la rencontre faite avec elle.<br>Au-delà de ses capacités de jeu considérées par son public plus que certaines, offertes.<br>Quiconque la connaissant le sait : elle se tuerai si elle ne jouait pas.<br>Au-delà donc de toute cette écrasante vérité d'être bien face à une comédienne, livrée en pâture aux publics, comme consciente de la profonde bienveillance qu'à son tour, elle reçoit.<br>Au-delà, c'est son esthétique de l'humour, sa grandiosité de « la bêtise » clownesque, son enthousiasme pour rendre la fée (modèle illusoire de la féminité) une tortionnaire empathique. Le tout offert par sa joie du jeu à entrer dans le paradoxe de l'existence qui suggère que ce choix, c'est le bon. »<br>**jo**

retrouvez par exemple [Julie Hercberg](https://www.facebook.com/jherc.herc)  avec les compagnies  [les petites secousses](http://www.petitessecousses.fr/Les), [digame](http://www.compagniedigame.com/), ou la compagnie[les labyrinthes](https://www.facebook.com/leslabyrinthes/Les). ainsi que dans des courts métrage comme [Tomatic]https://vimeo.com/279399629 de [Christophe M. Saber](https://www.christophemsaber.com/) produit par **canal+** ou des séries comme [Alexandra Ehle](https://television.telerama.fr/tele/films/alexandra-ehle,123703876,videos.php) diffuser sur [France 3](https://www.france.tv/france-3/). Elle continue toujours de se former auprès des plus grands comme [Castle](https://www.itnewyork.org/it-new-york/Robert). L'association TEDUA et tout ses membres sont fiers d'avoir parmi eux une comédienne de cette envergure. Merci !

## Thibaud Ghidone

![photo thibaud ghidone](/images/thibaud pour présentation.jpg)

Thibaud est clown. Il est le collègue de toujours. Celui qui traverse avec l'autre tous les échecs et tous les rater, les bons comme les mauvais. Avec ce spectacle, Thibaud offre toute sa sensibilté comme collé au clown qu'il est.

## Jonathan Dupui

![photo jonathan dupui](/images/jonathan pour présentation.jpg)

Jonathan a fait le conservatoire de Mérignac de septembre 2006 à juillet 2009. Tout en travaillant le jeu, à écrire et mettre en scène différents projets, Gérard David le lance en tant que professeur de théâtre à la fois pour les adultes et pour les enfants, au sein de la compagnie les Labyrinthes dès 2007.
Il a vu en lui une spontanéité et un sens de la pédagogie que Jonathan a su exploiter, puisqu’aujourd’hui, son enseignement permet aux débutant-e-s comme au comédien-ne-s professionnel-le-s, de découvrir un nouvel espace : celui de l’excatitude de où se trouve le jeu.

Sur ces 12 dernières années, il écrit 11 pièces et met en scène 14 spectacles dont un Opéra. Il joue dans 18 pièces et fréquente une cinquantaine de lieux différents à travers la France. Enfin, il a diffusé son enseignement à 150 personnes environ, enfants, adolescents et adultes confondus.

Il crée sa compagnie de théâtre UBU&tout en 2013 pour porter ses oeuvres et son enseignement.

C’est en septembre 2016 qu’il commence, au sein de sa compagnie, les ateliers des jeudis soirs avec 5 élèves dans les locaux de la Compagnie du Chien Dans les Dents.

La rentrée scolaire suivante, en septembre 2017, il continue les ateliers dans les locaux de la compagnie Du Chien avec 10 élèves cette fois-ci, s’associe avec Anne Lucie Dumay et lancent ensemble les premiers stages pour adultes un weekend par mois. En milieu d’année voyant l’engouement des personnes face au début de l’association, nous avons décidé de mettre en place des ateliers clown.

Dans leur collaboration, deux rôles se des-
sinent : Jonathan est à la direction artistique et Anne Lucie est en charge de la diffusion et de la production pour le développement de notre association TEDUA.

Pour l’année prochaine, 31 élèves-adhérents se tiennent déjà prêts :
pour continuer l’aventure à travers l’invitation au jeu de jonathan,
pour collaborer à la vie associative de TEDUA,
pour diffuser collectivement au travers des réseaux sociaux et les différentes plateformes numériques les oeuvres et les formations de leur association.

### Skins
